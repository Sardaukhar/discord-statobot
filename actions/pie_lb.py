import datetime
import os

import discord

import math
from matplotlib import pyplot as plt

from actions.action import AbstractAction
from utils.channels import text_channels
from utils.date import parse_date
from utils.history_memoization import get_history


class PieLeaderboard(AbstractAction):

    @staticmethod
    def command():
        return "!pie"

    @staticmethod
    def command_short():
        return "!pie"

    @staticmethod
    def help_description():
        return "Afficher le leaderboard en camembert"

    @staticmethod
    def help_args():
        return ["", "jj/mm/aaaa"]

    @staticmethod
    async def on_call(message, client):
        splitted = message.content.split()
        if len(splitted) == 1:
            async with message.channel.typing():
                await PieLeaderboard.send_pie(client, message.guild, message.channel, datetime.date.today())
        elif len(splitted) == 2:
            date = parse_date(splitted[1])
            if date is None:
                await message.channel.send("*Invalid date format*")
            else:
                async with message.channel.typing():
                    await PieLeaderboard.send_pie(client, message.guild, message.channel, date)
        else:
            await message.channel.send("*Invalid argument amount*")

    @staticmethod
    async def send_pie(client, guild, channel, date):
        day_info = {}

        for chan in text_channels(guild):
            try:
                for message in await get_history(chan, date):
                    if message.author == client.user:
                        continue
                    if message.author not in day_info:
                        day_info[message.author] = 0
                    day_info[message.author] += 1
            except:
                pass

        fig, ax = plt.subplots(figsize=(10, 4), subplot_kw=dict(aspect="equal"))

        users, values = [], []
        keys = sorted(day_info.keys(), key=lambda key: day_info[key], reverse=True)

        for user in keys:
            if user.nick and all(ord(char) < 128 for char in user.nick):
                users.append(user.nick + " (" + str(day_info[user]) + ")")
            else:
                users.append(str(user)[:-5] + "(" + str(day_info[user]) + ")")
            values.append(day_info[user])

        total = sum(values)
        truncated_users = [u for i, u in enumerate(users) if values[i] > total/15]
        truncated_values = [v for i, v in enumerate(values) if values[i] > total/15]

        explode = [0.]*len(truncated_values) + [0.08]*(len(values) - len(truncated_values))
        legend = ["_nolegend_"]*len(truncated_values) + users[len(truncated_users):]

        if total - sum(truncated_values) > 0:
            truncated_values.append(total - sum(truncated_values))
            truncated_users.append("Autres (" + str(truncated_values[-1]) + ")")

        wedges, texts, autotexts = ax.pie(values, startangle=-40, autopct=show_text, explode=explode)

        bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
        kw = dict(arrowprops=dict(arrowstyle="-"), bbox=bbox_props, zorder=0, va="center")

        end = False
        longest_name_length = 0
        for i, u in enumerate(truncated_users):
            p = wedges[i]
            if day_info[keys[i]] < total/15:
                ang = (p.theta2 + 40 - 360) / 2 - 40
                end = True
            else:
                ang = (p.theta2 - p.theta1) / 2. + p.theta1
            y = math.sin(math.radians(ang))
            x = math.cos(math.radians(ang))
            if x > 0 and len(u) > longest_name_length:
                longest_name_length = len(u)

            horizontal_alignment = "right" if x < 0 else "left"
            connection_style = "angle,angleA=0,angleB={}".format(ang)
            kw["arrowprops"].update({"connectionstyle": connection_style})

            ax.annotate(u, xy=(x, y), xytext=(1.35 * (-1 if x < 0 else 0 if x == 0 else 1), 1.4 * y),
                        horizontalalignment=horizontal_alignment, **kw)
            if end:
                break

        plt.setp(autotexts, size='x-small')
        if len(truncated_users) != len(users):
            ax.legend(wedges, legend, title='Autres ({:.1f}%)'.format(truncated_values[-1]/total*100),
                      loc="lower left", bbox_to_anchor=(1.8, 0))

        plt.savefig("activity.png", bbox_inches="tight")
        pic = open("activity.png", "rb")
        discord_pic = discord.File(pic)
        txt = "Voici le camembert d'activité du " + datetime.datetime.strptime(str(date), '%Y-%m-%d').strftime(
            '%d/%m/%Y')
        await channel.send(content=txt, file=discord_pic)
        discord_pic.close()
        pic.close()
        os.remove("activity.png")
        plt.clf()


def show_text(val):
    # val is a percentage, arbitrary threshold
    if val >= 5:
        return "{:.1f}%".format(val)
    else:
        return ""
