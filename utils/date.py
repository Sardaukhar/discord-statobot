import datetime
import re


def parse_date(date_str):
    if date_str == "yd" or date_str == "yesterday":
        date = datetime.datetime.now() - datetime.timedelta(hours=23)
        return datetime.date(day=date.day, month=date.month, year=date.year)

    regex = '([0-9][0-9]?)/([0-9][0-9]?)/([0-9][0-9][0-9]?[0-9]?)'
    search = re.search(regex, date_str)
    if not search:
        return None
    day = search.group(1)
    mon = search.group(2)
    yer = search.group(3)
    date = datetime.date(day=int(day), month=int(mon), year=int(yer))
    return date